import React, { useEffect, useState } from "react";

function VehicleModelForm() {
    const [manufacturers, setManufacturers] = useState([]);
    const [name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [pictureUrl, setpictureUrl] = useState('');

    const handleSubmit= async (event) =>{
        event.preventDefault();
        const data={};

        data.name = name; 
        data.manufacturer_id= manufacturer;

        data.picture_url = pictureUrl;
        const modelUrl= 'http://localhost:8100/api/models/';
        const fetchConfig = {

            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            const success = document.getElementById('success');
            success.className = 'text-center';
            const failure = document.getElementById('failure');
            failure.className = 'text-center d-none';

            setName('');
            setpictureUrl('');
            setManufacturer('');
        } else {
            const success = document.getElementById('success');
            success.className = 'text-center d-none'; 
            const failure = document.getElementById('failure');
            failure.className = 'text-center'; 
        }
    }
    

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handlepictureUrlChange = (event) => {
        const value = event.target.value;
        setpictureUrl(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }



    useEffect(()=>{
        const fetchData = async () => {
            const url = 'http://localhost:8100/api/manufacturers/';
            const response = await fetch(url);
            

            if (response.ok){
                const data = await response.json();
                setManufacturers(data.manufacturers)

            }
        }
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a vehicle model</h1>
                <form onSubmit={handleSubmit} id="create-model-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} placeholder="Name" required type="text" name="Name"  value={name} id="Name" className="form-control"/>
                    <label htmlFor="name">Model name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlepictureUrlChange} placeholder="pictureUrl" required type="text" name="pictureUrl"  value={pictureUrl} id="pictureUrl" className="form-control"/>
                    <label htmlFor="pictureUrlr">Picture Url</label>
                </div>
                <div className="mb-3">
                    <select onChange= {handleManufacturerChange} required name="manufacturer" value={manufacturer}  id="manufacturer" className="form-select">
                        <option value="">Choose a manufacturer</option>
                        {manufacturers.map(manufacturer => {
                            return (
                                <option key={manufacturer.id} value={manufacturer.id}>
                                    {manufacturer.name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
                <h4 id='success' className='text-center d-none'>Model successfully created!</h4>
                <h5 id='failure' className='text-center d-none'>Failed to create Model. Please try again.</h5>
            </div>
            </div>
        </div>
    );
}

export default VehicleModelForm;
