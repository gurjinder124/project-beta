import React, {useState} from 'react';


function ManufacturerForm() {
    const [name, setName] = useState('');
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            const success = document.getElementById('success');
            success.className = 'text-center';
            const failure = document.getElementById('failure');
            failure.className = 'text-center d-none';
            setName('');
        }
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} placeholder="Manufacturer name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="first_name">Manufacturer name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    <h4 id='success' className='text-center d-none'>Manufacturer successfully created!</h4>
                    <h5 id='failure' className='text-center d-none'>Failed to create Manufacturer. Please try again.</h5>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm;
