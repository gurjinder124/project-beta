import React, {useEffect, useState} from "react";


function AutomobileList() {
  const [autos, setAutos] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/automobiles/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setAutos(data.autos);
      }
    } catch (e) {
      console.error(e);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  function convertBool(boolean) {
    if (boolean === true) {
        return (
            <td>Yes</td>
        );
    }
    return (
        <td>No</td>
    );
  }

  return (
    <div>
      <h1 style={{ marginTop: "15px", fontSize: "36px", color: "#333" }}>Automobiles</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {autos.map(auto => {
            return (
              <tr key={ auto.href }>
                <td>{ auto.vin }</td>
                <td>{ auto.color }</td>
                <td>{ auto.year }</td>
                <td>{ auto.model.name }</td>
                <td>{ auto.model.manufacturer.name }</td>
                { convertBool(auto.sold) }
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobileList;
