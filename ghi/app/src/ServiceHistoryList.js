import React, {useEffect, useState} from "react";


function HistoryList() {
    const [appointments, setAppointments] = useState([]);
    const [vin, setVin] = useState('');

    const getDate = (dateString) => {
        let date = new Date(dateString);
        let formattedDate = date.toLocaleDateString('en-us', { year:"numeric", month:"numeric", day:"numeric"}) 
        return formattedDate;
    }

    const getTime = (timeString) => {
        let time = new Date(timeString);
        let formattedTime = time.toLocaleTimeString('en-us', { hour:"numeric", minute:"numeric", second:"numeric",hour12:true}) 
        return formattedTime;
    }



    const handleSearch = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        try{
            const response = await fetch(url);
            if (response.ok){
                const data = await response.json();
                setAppointments(data.appointments)
            }
        } catch (e) {
            console.error(e);
        }
        if (vin !== '') {
            let filteredAppointments = appointments.filter(appointment => appointment.vin.startsWith(vin));
            setAppointments(filteredAppointments)
        }
    }
    

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    
    
    useEffect(()=>{
        const fetchData = async () => {
            const url = 'http://localhost:8080/api/appointments/';
            const vinUrl = 'http://localhost:8080/api/vin/';
            try{
                const response = await fetch(url);
                if (response.ok){
                    const data = await response.json();
                    const vinResponse = await fetch(vinUrl);
                    if (vinResponse.ok){
                        const vinData = await vinResponse.json();
                        let vipVins = []
                        vinData.autos.map(auto => {
                            vipVins.push(auto.vin)
                        });
                        data.appointments.map(appointment => {
                            if (vipVins.includes(appointment.vin)) {
                                appointment.vip = true;
                            }
                        })
                        setAppointments(data.appointments)
                    } 
                }
    
            } catch (e) {
                console.error(e);
            }
    }
        fetchData();
    }, []);
    



    return (
        <div>
        <h1 style={{ marginTop: "15px" }}>Service History</h1>
        <div className="input-group mb-3">
        <input type="text" onChange={handleVinChange} className="form-control" placeholder="Search by VIN..." value={vin} aria-describedby="basic-addon2"/>
        <div className="input-group-append">
            <button onClick={() => handleSearch()} className="btn btn-outline-secondary" type="button">Search</button>
        </div>
        </div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            {appointments.map(appointment => {
                return (
                <tr key={ appointment.id }>
                    <td>{ appointment.vin}</td>
                    <td>{ appointment.vip ? "YES" : "NO"}</td>
                    <td>{ appointment.customer}</td>
                    <td>{ getDate(appointment.date_time)}</td>
                    <td>{ getTime(appointment.date_time) }</td>
                    <td>{ appointment.technician.first_name}</td>
                    <td>{ appointment.reason}</td>
                    <td>{ appointment.status}</td>
                </tr>
                );
            })}
            </tbody>
        </table>
        </div>
    );
}

export default HistoryList;
