from django.urls import path

from .views import (
    api_technicians,api_technician_delete,api_appointments,api_appointment_delete,api_appointment_cancel,api_appointment_finish,api_vin
)

urlpatterns = [
    path(
        "technicians/",
        api_technicians,
        name="api_technicians",
    ),
    path(
        "technicians/<int:pk>/",
        api_technician_delete,
        name="api_technician",
    ),
    path(
        "appointments/",
        api_appointments,
        name="api_appointments",
    ),
    path(
        "appointments/<int:pk>",
        api_appointment_delete,
        name="api_appointment",
    ),
    path(
        "appointments/<int:pk>/cancel/",
        api_appointment_cancel,
        name="api_appointment_cancel",
    ),
    path(
        "appointments/<int:pk>/finish/",
        api_appointment_finish,
        name="api_appointment_finish",
    ),
    path(
        "vin/",
        api_vin,
        name="api_vin",
    ),
]
